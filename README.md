# cleanup-mastodon-users.sh
Script to remove inactive users on your Mastodon server. This is a small cleanup script for Mastodon node admins to help with forgotten or dead accounts. 

The script does two things: 
1. delete all users after 2 weeks that haven't logged in at all and didn't post anything.
2. search for accounts that haven't logged in for 6 months, send them a reminder mail, deactivate accounts that haven't logged within 7 months and delete those deactivated users in a `--cron` run. 

# Installation
* save the script in home directory of Mastodon (eg. ~/bin, not under /root, maybe /usr/local/bin)
* make the script executable (e.g. `chown mastodon:mastodon bin/cleanup-mastodon-users.sh && chmod u+rx bin/cleanup-mastodon-users.sh)`
* create a config file at `$HOME/.cleanup-mastodon-users.conf` by calling the script with `--init` argument and edit the file to fit your needs
* create a crontab to execute the script, e.g.: `20 8    * * 4   /home/mastodon/bin/cleanup-mastodon-users.sh --dowhatimean` to execute the script every week. In the example it is executed every Thursday (4th day of week) at 8:20 in the morning. 
* for the first runs you should set `UPPERLIMIT` to a high number and keep the deleting of the user commented out (`${mastodonpath}/bin/tootctl accounts delete "${username}"`). After the first few runs you can reduce `UPPERLIMIT` to a lower setting and repeat this until you reached the final value. You shouldn't have too many weeks between `LOWERLIMIT` and `UPPERLIMIT`. Sensible values might be 6 and 7 months or 12 and 13 months or so.  
* change the variables at the begin of the script to your site settings. 

# Usage
```
Usage:
--init          : create bin/cleanup-mastodon-users.sh/.cleanup-mastodon-users.conf config file.
 --dry-run      : make a dry-run, no deletion will be done, no mails are sent.
 --cron         : delete deactivated users in a regularly cron run step by step to avoid mass flooding.
 --dowhatimean  : add this option if you really want to delete users.
 ```

# Other useful tips
* test the script by using `--dry-run` argument with the script
* you can change the mail command from `-b ${siteadmin} -- ${usermail}` to `-- ${siteadmin}` and comment out all lines with `bin/console user delete` statement for testing runs

# Donations
If you want to support my work, you can donate via LiberaPay: 

[![Donate via LiberaPay](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Windfluechter/donate) or on [Patreon](https://patreon.com/nerdculture_de)
